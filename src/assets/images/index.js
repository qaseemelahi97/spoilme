export default {
    logo:require('./logo.png'),
    mapIstanbul:require('./mapIstanbul.png'),
    soda:require('./soda.png'),
    mysteryBox:require('./mysteryBox.png'),
    yoga:require('./yoga.png'),
    coin:require('./coin.png'),
    burger:require('./hamburger.png'),
    people1:require('./people1.png'),
}